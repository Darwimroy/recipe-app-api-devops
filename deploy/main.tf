terraform {
  backend "s3" {
    bucket  = "shuhai-backend-terraform"
    key     = "recipe-app.tfstate"
    region  = "us-east-1"
    encrypt = true

    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region = "us-east-1"
  #   version = "~>4.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    project     = var.project
    Owner       = var.contact
    ManageBy    = "Terraform"
  }
}