data "aws_ami" "redhat_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["RHEL-9.0.0_HVM-*-x86_64-43-Hourly2-GP2"]
  }
  owners = ["amazon"]
}

resource "aws_instance" "bastion" {
  ami           = data.aws_ami.redhat_linux.id
  instance_type = "t2.micro"

  #   tags = {
  #     Name = "${local.prefix}-bastion"
  #   }
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}

